# Wifikill for linux
A simple shell script that allows you to kill wifi for specific devices on your network(or all if you'd like)

# Options
```
./wifikill --list 				| Lists devices from network
./wifikill --killall 			| Kills all devices from network
./wifikill --kill [target(s)]	| Kills specific target(s)
./wifikill --help				| Displays this help
```